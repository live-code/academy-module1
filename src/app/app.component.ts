import { Component, ViewContainerRef } from '@angular/core';
import { HelloComponent } from './core/hello/hello.component';
import {Company, Guest, GuestAddress} from "./model/guest";

export type Page = 'shop' | 'members' | 'users' | 'contacts' | 'http';
export type Theme = 'dark' | 'light';

@Component({
  selector: 'ang-root',
  template: `
    <div
      class="my-navbar myActive d-flex justify-content-between"
      [ngClass]="{'dark': theme === 'dark', 'light': theme === 'light'}"
    >
      <div>
        <button (click)="changePage('shop')">shop</button>
        <button (click)="page = 'members'">members</button>
        <button (click)="page = 'users'">users</button>
        <button (click)="page = 'contacts'">contatti</button>
        <button (click)="page = 'http'">http</button>
      </div>

      <div>
        <button (click)="changeTheme('dark')">dark</button>
        <button (click)="changeTheme('light')">light</button>
      </div>
    </div>

    <div [ngSwitch]="page" class="container mt-2">
      <ang-shop *ngSwitchCase="'shop'"></ang-shop>
      <ang-members *ngSwitchCase="'members'"></ang-members>
      <ang-users *ngSwitchCase="'users'"></ang-users>
      <ang-contacts *ngSwitchCase="'contacts'"></ang-contacts>
      <ang-http-example *ngSwitchCase="'http'"></ang-http-example>
    </div>
  `,
  styles: [`
    .my-navbar {padding: 10px}
    .dark { background-color: gray}
    .light { background-color: #efefef}
  `]
})
export class AppComponent {
  page: Page = 'http'
  theme: Theme = 'light';

  constructor() {
    const themeFromLocalStorage: string | null = localStorage.getItem('theme');
    if (themeFromLocalStorage) {
      this.theme = themeFromLocalStorage as Theme
    }
  }

  changePage(nextPage: Page) {
    this.page = nextPage;
  }

  changeTheme(theme: Theme) {
    this.theme = theme;
    localStorage.setItem('theme', theme)
  }

}
