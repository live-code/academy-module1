
export interface Geo {
  lat: string;
  lng: string;
}

export interface GuestAddress {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: Geo;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface Guest {
  id: number;
  name: string;
  username: string;
  email: string;
  address: GuestAddress;
  phone: string;
  website: string;
  company: Company;
  image: Image
}

// image.ts
export interface Image {

}
