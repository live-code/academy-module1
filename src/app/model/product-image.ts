
export interface ProductImage {
  value: string,
  width: number
}
