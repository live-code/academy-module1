export interface User {
  id: number;
  name: string;
  surname: string;
  age?: number;
  gender?: 'M' | 'F';
  visible?: boolean;
}
