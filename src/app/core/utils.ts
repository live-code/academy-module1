import { User } from '../model/user';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})
export class UtilsService {
  isMaggiorenne(user: User) {
    return user.age && user.age > 30
  }
}
