import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ang-hello',
  template: `
    <p>
      hello works! <span [innerHTML]="name"></span>
    </p>
  `,
  styles: [
  ]
})
export class HelloComponent {
  @Input() name!: string;
}
