import { Component } from '@angular/core';
import { CartItem, Product } from '../../model/product';

@Component({
  selector: 'ang-shop',
  template: `
    
    <div class="row">
      <div class="col" *ngFor="let product of products">
        <div class="card">
          <div class="card-header">{{product.name}}</div>
          
          <div class="card-body">
            <img width="100%" [src]="product.img" [alt]="product.name">
            
            <div class="d-flex justify-content-around mt-2" *ngIf="product.variants">
              <div
                style="width: 30px; height: 30px; border: 1px solid black"
                *ngFor="let variant of product.variants"
                [style.background-color]="variant"
                (click)="addToCart(product, variant)"
              >
              </div>
            </div>
          </div>
          
          <!--<button class="btn btn-primary" (click)="addToCart(product)">ADD TO CART</button>-->
        </div>
      </div>
    </div>
    
    <div *ngIf="cart.length">
      <h1>Cart: {{cart.length}} products</h1>
      <li *ngFor="let item of cart; let i = index">
         {{item.product.name}} - {{item.variant}}
        <i class="fa fa-trash" (click)="isProductDeleting = item"></i>
      </li>
      <hr>
      total cost: € {{getTotal()}}
    </div>
    
    <div class="my-modal" *ngIf="isProductDeleting">
      Sei sicuro di voler rimuoverlo?
      <button (click)="removeFromCart(isProductDeleting)">si</button>
      <button (click)="isProductDeleting = null">no</button>
    </div>
  `,
  styles: [`
    .my-modal {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: white;
    }
  `]
})
export class ShopComponent {
  products: Product[] = [
    {
      id: 1,
      name: 'Tshirt XYZ',
      cost: 50,
      img: 'https://picsum.photos/id/237/400/300',
      variants: ['red', 'purple', 'cyan'],
    },
    {
      id: 2,
      name: 'Pants ABC',
      cost: 30,
      img: 'https://picsum.photos/id/238/400/300',
      variants: ['red', 'lightgreen'],
    },
    {
      id: 3,
      name: 'Maglione XYZ',
      cost: 20,
      img: 'https://picsum.photos/id/239/400/300',
      variants: ['yellow', 'orange'],
    },
  ];
  cart: CartItem[] = [];
  isProductDeleting: CartItem | null = null;

  addToCart(product: Product, variant: string) {
    this.cart.push({ product: product, variant: variant})
    /*const alreadyAdded = this.cart.find((p) =>  p.id === product.id)
    if (!alreadyAdded) {
      this.cart.push(product)
    }*/
  }

  getTotal() {
    return this.cart.reduce((acc, item) => acc + item.product.cost, 0)
  }

  removeFromCart(itemToRemove: CartItem) {
    // .... async
    this.isProductDeleting = null;
    const index = this.cart.findIndex(item => {
      return item.product.id === itemToRemove.product.id &&
        item.variant === itemToRemove.variant
    })
    this.cart.splice(index, 1)
  }
}
