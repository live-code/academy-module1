import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from "../../../model/post";

@Component({
  selector: 'ang-post-list-item',
  template: `
    <li
      (click)="selectPost.emit(post)"
      class="list-group-item myActive"
      [ngClass]="{active: post.id === active?.id}"
    >
      {{post.id}} -
      {{post.title}}
      <i class="fa fa-trash" (click)="deleteHandler(post, $event)"></i>
      <i
        class="fa fa fa-arrow-circle-down"
        (click)="isOpened = !isOpened"
      ></i>

      <div *ngIf="isOpened">
        {{post.body}}
        <ac-static-map [value]="post.city"></ac-static-map>
      </div>
    </li>
  `,
  styles: [`
   /* .myActive {
      background-color: orange;
    }*/
  `]
})
export class PostListItemComponent {
  @Input() post!: Partial<Post>;
  @Input() active: Partial<Post> | null = null;
  @Output() selectPost = new EventEmitter<Partial<Post>>()
  @Output() deletePost = new EventEmitter<{post: Partial<Post>, value: number}>()
  isOpened = false;

  deleteHandler(postToDelete: Partial<Post>, event: MouseEvent) {
    event.stopPropagation();
    this.deletePost.emit({ post: postToDelete , value: 123 })
  }

}
