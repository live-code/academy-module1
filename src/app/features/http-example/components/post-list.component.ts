import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {Post} from "../../../model/post";

@Component({
  selector: 'ang-post-list',
  //encapsulation: ViewEncapsulation.None,
  // encapsulation: ViewEncapsulation.ShadowDom,
  template: `
    <h4 class="myActive">{{posts.length}} posts</h4>
    <ang-post-list-item
      *ngFor="let p of posts"
      [post]="p"
      [active]="active"
      (selectPost)="selectPost.emit($event)"
      (deletePost)="deletePost.emit($event)"
    >
    </ang-post-list-item>
  `,
  styles: [`
    .myActive {
      background-color: blue !important;
    }
  `]
})
export class PostListComponent {
  @Input() posts: Partial<Post>[] = [];
  @Input() active: Partial<Post> | null = null;
  @Output() selectPost = new EventEmitter<Partial<Post>>()
  @Output() deletePost = new EventEmitter<{post: Partial<Post>, value: number}>()

}
