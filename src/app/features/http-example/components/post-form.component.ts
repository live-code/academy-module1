import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from "../../../model/post";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'ang-post-form',
  template: `
    <form #f="ngForm" (submit)="save.emit(f)">
      <input
        placeholder="add post title and press enter"
        class="form-control" type="text"
        [ngModel]="selectedPost?.title"
        name="title"
      >

      <button
        *ngIf="selectedPost" type="button" class="btn"
        (click)="reset.emit(f)">CLEAN</button>
    </form>
  `,
})
export class PostFormComponent {
  @Input() selectedPost: Partial<Post> | null = null;
  @Output() save = new EventEmitter<NgForm>()
  @Output() reset = new EventEmitter<NgForm>()
}
