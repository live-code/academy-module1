import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ang-post-error',
  template: `
    <div class="alert alert-danger" *ngIf="value">
      c'è un errore {{value}}
      <i class="fa fa-times" (click)="close.emit()"></i>
    </div>
  `,
})
export class PostErrorComponent {
  @Input() value: boolean = false;
  @Output() close = new EventEmitter()
}
