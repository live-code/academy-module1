import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ac-static-map',
  template: `
    <img
      *ngIf="value"
      width="100%"
      [src]="getImg()" alt="">
    {{render()}}

  `,
})
export class StaticMapComponent  {
  @Input() value: string | undefined;

  getImg() {
    return `https://maps.googleapis.com/maps/api/staticmap?center=${this.value}&zoom=9&size=1200x400&key=AIzaSyBzQurvW6YciltmMcXNZwKo5njyFIn_f8I`
  }

  render() {
    console.log('render')
  }
}
