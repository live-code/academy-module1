import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../../model/post";
import {NgForm} from "@angular/forms";
import {PostListComponent} from "./components/post-list.component";
import {StaticMapComponent} from "./components/static-map.component";

@Component({
  selector: 'ang-http-example',
  template: `
     <ang-post-error
      [value]="error"
      (close)="error = false"
    ></ang-post-error>

    <ang-post-form
      [selectedPost]="selectedPost"
      (save)="save($event)"
      (reset)="reset($event)"
    ></ang-post-form>

    <ang-post-list
      [posts]="posts"
      [active]="selectedPost"
      (selectPost)="selectHandler($event)"
      (deletePost)="deleteHandler($event)"
    ></ang-post-list>
  `,
})
export class HttpExampleComponent {
  posts: Partial<Post>[] = [];
  selectedPost: Partial<Post> | null = null;
  error: boolean = false;

  constructor(
    private http: HttpClient,
    private view: ViewContainerRef,
  ) {
    // --------x > // marble diagrams
    this.http.get<Post[]>('http://localhost:3000/posts')
      .subscribe(
        (res) => {
          this.posts = res;
        },
        err => {
          this.error = true;
        }
      )
  }

  save(form: NgForm) {
    if (this.selectedPost?.id) {
      this.editHandler(form)
    } else {
      this.addHandler(form)
    }
  }

  editHandler(form: NgForm) {
    this.http.patch<Pick<Post, 'id' | 'title'>>(`http://localhost:3000/posts/${this.selectedPost?.id}`, form.value)
      .subscribe(res => {
        const index = this.posts.findIndex(post => post.id === this.selectedPost?.id)
        this.posts[index] = res;
      })
  }

  addHandler(form: NgForm) {
    this.http.post<Pick<Post, 'id' | 'title'>>('http://localhost:3000/posts', form.value)
      .subscribe(res => {
        this.posts.push(res);
        // this.posts = [...this.posts, res]
        form.reset();
      })
  }

  deleteHandler(obj: {post: Partial<Post>, value: number}) {
    this.error = false;

    this.http.delete('http://localhost:3000/posts/' + obj.post.id)
      .subscribe(
        () => {
          const index = this.posts.findIndex(post => post.id === obj.post.id)
          this.posts.splice(index, 1)

          if (this.selectedPost?.id === obj.post.id) {
            this.selectedPost = null;
          }
        },
        err => this.error = true
      )

  }

  selectHandler(post: Partial<Post>) {
    console.log('select handler')
    this.selectedPost = post;
  }

  reset(form: NgForm) {
    this.selectedPost = null;
    form.reset();
  }
}
