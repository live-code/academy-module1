import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from '../../model/user';

@Component({
  selector: 'ang-users',
  template: `
    <div *ngIf="error">Campi obbligatori</div>
    <input type="text" #nameRef placeholder="name" (keyup.enter)="addUser()">
    <input type="text" #surnameRef placeholder="surname" (keyup.enter)="addUser()">

    <li
      *ngFor="let user of users"
      (click)="deleteUser(user)"
      class="list-group-item"
    >
      {{user.name}} - {{user.surname}}
    </li>
  `,
})
export class UsersComponent {
  @ViewChild('nameRef') nameInput!: ElementRef<HTMLInputElement>;
  @ViewChild('surnameRef') surnameInput!: ElementRef<HTMLInputElement>;

  users: User[] = [
    { id: 1, name: 'pippo', surname: 'ciao ciao1' },
    { id: 2, name: 'pluto', surname: 'ciao ciao 2' },
    { id: 3, name: 'topolino', surname: 'ciao ciao3' },
  ];

  error: boolean = false;

  ngAfterViewInit() {
    console.log(this.nameInput.nativeElement)
    this.nameInput.nativeElement.focus()
  }

  addUser() {
    const name = this.nameInput.nativeElement.value;
    const surname = this.surnameInput.nativeElement.value;

    if (name !== '' && surname !== '') {
      this.error = false;
      this.users.push({ id: 4, name, surname, gender: 'M'}) // object concise syntax
      this.nameInput.nativeElement.value = ''
      this.surnameInput.nativeElement.value = ''
      this.nameInput.nativeElement.focus();
    } else {
      this.error = true;
    }

  }

  deleteUser(userToDelete: User) {
    const index = this.users.findIndex(u => u.id === userToDelete.id)
    this.users.splice(index, 1);
  }

  giveFocus(nameRef: HTMLInputElement) {
    nameRef.focus();
  }
}
