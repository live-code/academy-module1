import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { User } from '../../model/user';

@Component({
  selector: 'ang-members',
  template: `

    <form #f="ngForm" (submit)="save(f)">
      <div *ngIf="inputName.errors?.['required']">campo obbligatorio</div>
      <div *ngIf="inputName.errors?.['minlength']">
        campo troppo corto. Mancano
        {{ inputName.errors?.['minlength']['requiredLength']  - inputName.errors?.['minlength']['actualLength'] }} caratteri
      </div>

      <input
        placeholder="name"
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid }"
        type="text" [ngModel]="selectedUser?.name" name="name" required minlength="5" #inputName="ngModel">

      <div class="progressbar" [style.width.%]="getPerc(inputName.errors?.['minlength'])"></div>

      <input
        placeholder="surname"
        class="form-control my-2"
        [ngClass]="{'is-invalid': inputSurname.invalid && f.dirty, 'is-valid': inputSurname.valid }"
        type="text" [ngModel]="selectedUser?.surname" name="surname"
        required minlength="3"  #inputSurname="ngModel">

      <div class="progressbar" [style.width.%]="getPerc(inputSurname.errors?.['minlength'])"></div>


      <select required class="form-control" [ngModel]="selectedUser?.gender" name="gender" >
        <option [ngValue]="null">select gender</option>
        <option [ngValue]="'M'">male</option>
        <option ngValue="F">female</option>
        <option [ngValue]="{id: 123}">obj</option>
      </select>

      <input type="checkbox" [ngModel]="selectedUser?.visible" name="visible">

      <button type="button" (click)="resetForm(f)">reset form</button>

      <button type="submit" [disabled]="f.invalid">
        {{selectedUser ? 'EDIT' : 'ADD'}}
      </button>
    </form>

    <li
      *ngFor="let user of users"
      (click)="selectUser(user)"
      class="list-group-item"
      [ngClass]="{
        active: user.id === selectedUser?.id
      }"
    >
      <div class="d-flex justify-content-between align-items-center">
        <div>
          <div style="font-weight: bold">{{user.name}}</div>
          <em class="text-secondary">{{user.surname}} - {{user.gender}}</em>
        </div>
        <div>
          <i class="fa fa-trash" (click)="deleteHandler(user)"></i>
        </div>
      </div>

    </li>

  `,
  styles: [`
    .progressbar {
      width: 100%;
      height: 10px;
      background-color: orange;
      transition: 1s all cubic-bezier(0.34, 1.56, 0.64, 1);
    }
  `]
})
export class MembersComponent {
  selectedUser: User | null = null;

  users: User[] = [
    { id: 1, name: 'pippo', surname: 'ciao ciao', gender: 'M', visible: false },
    { id: 2, name: 'pluta', surname: 'ciao ciao', gender: 'F', visible: true },
    { id: 3, name: 'topolino', surname: 'ciao ciao', gender: 'M', visible: false },
  ];

  selectUser(user: User) {
    this.selectedUser = user;
  }

  save(form: NgForm) {
    console.log(form.value)
    if (this.selectedUser) {
      const index = this.users.findIndex(u => u.id === this.selectedUser?.id)
      this.users[index] = {...this.users[index], ...form.value}
    } else {
      const newUser = { ...form.value, id: Date.now() };
      this.users.push(newUser)
      this.selectedUser = newUser;
      form.reset();
    }
  }

  resetForm(form: NgForm) {
    form.reset();
    this.selectedUser = null;
  }

  deleteHandler(user: User) {
    const index = this.users.findIndex(u => u.id === user.id)
    this.users.splice(index, 1);
    if (user.id === this.selectedUser?.id ) {
      this.selectedUser = null;
    }
  }

  getPerc(obj: any) {
    if (obj) {
      return (obj.actualLength / obj.requiredLength) * 100;
    }
    return 0
  }
}

