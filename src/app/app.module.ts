import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ShopComponent } from './features/shop/shop.component';
import { MembersComponent } from './features/members/members.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { UsersComponent } from './features/users/users.component';
import { HelloComponent } from './core/hello/hello.component';
import { FormsModule } from '@angular/forms';
import { HttpExampleComponent } from './features/http-example/http-example.component';
import {HttpClientModule} from "@angular/common/http";
import { PostFormComponent } from './features/http-example/components/post-form.component';
import { PostListComponent } from './features/http-example/components/post-list.component';
import { PostErrorComponent } from './features/http-example/components/post-error.component';
import { PostListItemComponent } from './features/http-example/components/post-list-item.component';
import {StaticMapComponent} from "./features/http-example/components/static-map.component";

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
    MembersComponent,
    ContactsComponent,
    UsersComponent,
    HelloComponent,
    HttpExampleComponent,
    PostFormComponent,
    PostListComponent,
    PostErrorComponent,
    PostListItemComponent,
    StaticMapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
